import { callApi, getFavoriteMovie } from '../helpers/apiHelper';

class MovieService {
    async getMovieByKeywords(keywords: string, pageNumber: number) {
        const endpoint = 'search/movie';
        const query = 'query=' + keywords;
        const apiResult = await callApi(endpoint, 'GET', query, pageNumber);
        return apiResult;
    }

    async getMoviePopular(pageNumber: number) {
        const endpoint = 'movie/popular';
        const apiResult = await callApi(endpoint, 'GET', null, pageNumber);
        return apiResult;
    }

    async getMovieUpcoming(pageNumber: number) {
        const endpoint = 'movie/upcoming';
        const apiResult = await callApi(endpoint, 'GET', null, pageNumber);
        return apiResult;
    }

    async getMovieTopRated(pageNumber: number) {
        const endpoint = 'movie/top_rated';
        const apiResult = await callApi(endpoint, 'GET', null, pageNumber);
        return apiResult;
    }

    async getFavoriteMovie(id: string) {
        const endpoint = `movie/${id}`;
        const apiResult = await getFavoriteMovie(endpoint, 'GET');
        console.log(apiResult)
        return apiResult;
    }
}

export const movieService = new MovieService();