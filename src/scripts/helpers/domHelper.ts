import { CreateElementPropTypes } from './typeHelper';

export function createElement({ tagName, className, attributes }: CreateElementPropTypes): HTMLElement {

    const element = document.createElement(tagName);

    if (className) {
        const classNames = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    if (attributes) {
        Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));
    }

    return element;
}