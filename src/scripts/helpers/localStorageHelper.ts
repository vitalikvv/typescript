import { moviesList } from '../components/MoviesList';

export function setLocalStorage(e:Event, typeElement:string):void {
    const { id } = e.target as HTMLElement;
    if (id) {
    const svgElement:HTMLElement = document.getElementById(id) as HTMLElement;
    if(getLocalStorageItem(id)) {
        localStorage.removeItem(id);
        svgElement ? svgElement.setAttribute('fill', 'rgba(255,0,0,0.47)') : null
        if (typeElement === 'favorite') {
            svgElement?.closest('.col-12')?.remove()
            const svgElementInMainBlock:HTMLElement = document.getElementById(id) as HTMLElement;
            svgElementInMainBlock ? svgElementInMainBlock.setAttribute('fill', 'rgba(255,0,0,0.47)') : null
        }
    } else {
        localStorage.setItem(id,id);
        svgElement ? svgElement.setAttribute('fill', 'red') : null
    }
    }
}

export function getAllLocalStorageItems():void {
    const favoritesId:Array<string> | null = []
    for(let i=0; i<localStorage.length; i++) {
        const key:string | null = localStorage.key(i);
        key ? favoritesId.push(key) : null
    }
    if (favoritesId.length > 0) {
        moviesList.createFavoriteMoviesList(favoritesId)
    }
}

export function getLocalStorageItem(id:string):boolean {

    for (let i = 0; i < localStorage.length; i++){
        const finedId = localStorage.getItem(id);
        if (finedId === id) {
            return true
        }
    }
    return false
}