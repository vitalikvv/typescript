export type fullResponse = {
    page: number
    results: Array<filmFullData>
    total_pages: number
    total_results: number
};

export type filmFullData = {
    poster_path: string
    adult:boolean
    overview:string
    release_date:string
    genre_ids: Array<number>
    id:string
    original_title:string
    original_language:string
    title:string
    backdrop_path:string
    popularity:number
    vote_count:number
    video:boolean
    vote_average:number
};

export interface movie {
    title: string;
    backdrop_path: string;
    overview: string;
    id: string,
    release_date: string;
}


export type CreateElementPropTypes = {
    tagName: string,
    className: string,
    attributes?: {
            src?: string,
            id?: string
        }
}