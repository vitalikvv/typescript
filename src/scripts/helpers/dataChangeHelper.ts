import { filmFullData, movie } from './typeHelper';

export function filmsDataChange(filmsFullData:Array<filmFullData>):Array<movie> {
    return filmsFullData.map((film: filmFullData): movie => {
        const { title, overview, backdrop_path, id, release_date } = film
        return { title, overview, backdrop_path, id, release_date };
    });
}
