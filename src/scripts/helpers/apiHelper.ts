import { filmFullData, fullResponse } from './typeHelper';

const API_URL = 'https://api.themoviedb.org/3/';
const API_KEY = '?api_key=c1a709144dbbcf22457a57f1875b83cb&'

export async function callApi(endpoint: string, method: string, query: string | null, pageNumber?: number): Promise<fullResponse> {
    const url = pageNumber
                    ? API_URL + endpoint + API_KEY + query + '&page=' + pageNumber
                    : API_URL + endpoint + API_KEY + query
    const options = {
        method,
    };
    return fetch(url, options)
            .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
            .catch((error) => {
                throw error;
            });
}

export async function getFavoriteMovie(endpoint: string, method: string): Promise<filmFullData> {
    const url = API_URL + endpoint + API_KEY
    const options = {
        method,
    };
    return fetch(url, options)
        .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .catch((error) => {
            throw error;
        });
}