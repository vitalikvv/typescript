import { createElement } from '../helpers/domHelper';
import { getLocalStorageItem } from '../helpers/localStorageHelper';

export function createMovieItem(path: string, description:string, date:string, id:string, wrapper:string): HTMLElement {
    const mainWrapper = wrapper === 'main'
        ? createElement({
            tagName: 'div',
            className: 'col-lg-3 col-md-4 col-12 p-2',
        })
        : createElement({
            tagName: 'div',
            className: 'col-12 p-2',
        });

    const secondaryWrapper = createElement({
        tagName: 'div',
        className: 'card shadow-sm',
    });

    const imgItem = path
        ? createElement({
            tagName: 'img',
            className: 'card shadow-sm',
            attributes: {
                src: 'https://image.tmdb.org/t/p/original/' + path,
            }
        })
        : createElement({
            tagName: 'img',
            className: 'card shadow-sm',
        });

    const svgItem = createElement({
        tagName: 'svg',
        className: 'bi bi-heart-fill position-absolute p-2'
    });

    const isLiked = getLocalStorageItem(String(id));
    if (!isLiked) {
        svgItem.innerHTML =
            `<svg
                xmlns="http://www.w3.org/2000/svg"
                stroke="red"
                fill="#ff000078"
                width="50"
                height="50"
                class="bi bi-heart-fill position-absolute p-2"
                viewBox="0 -2 18 22"
            >
                <path
                    fill-rule="evenodd"
                    id=${id}
                    d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                />
            </svg>`;
    } else {
        svgItem.innerHTML =
            `<svg
                xmlns="http://www.w3.org/2000/svg"
                stroke="red"
                fill="red"
                width="50"
                height="50"
                class="bi bi-heart-fill position-absolute p-2"
                viewBox="0 -2 18 22"
            >
                <path
                    fill-rule="evenodd"
                    d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                    id=${id}
                />
            </svg>`;
    }

    const cardBody = createElement({
        tagName: 'div',
        className: 'card-body',
    })

    const cardText = createElement({
        tagName: 'p',
        className: 'card-text truncate',
    })

    cardText.innerText = description

    const smallWrapper = createElement({
        tagName: 'div',
        className: 'd-flex justify-content-between align-items-center',
    })

    const small = createElement({
        tagName: 'small',
        className: 'text-muted',
    })
    small.innerText = date
    smallWrapper.append(small)

    cardBody.append(cardText, smallWrapper)
    secondaryWrapper.append(imgItem, svgItem, cardBody)
    mainWrapper.append(secondaryWrapper)


    return mainWrapper;
}