import { movieService } from '../services/movie.service';
import { createMovieItem } from './MovieItem';
import { movie } from '../helpers/typeHelper';
import { filmsDataChange } from '../helpers/dataChangeHelper';

class MoviesList {
    async getMoviesByKeywords(pageNumber: number): Promise<void> {
        let isNeedReset = true;
        const queryString = (<HTMLInputElement>document.getElementById('search')).value;
        const result = await movieService.getMovieByKeywords(queryString, pageNumber);
        if (pageNumber > 1) {
            isNeedReset = false
        }
        if (result) {
            this.createMoviesList(filmsDataChange(result.results), isNeedReset)
        }
    }

    async getMoviesPopular(pageNumber: number): Promise<void> {
        let isNeedReset = true;
        const result = await movieService.getMoviePopular(pageNumber);
        if (result) {
        if (pageNumber > 1) {
            isNeedReset = false
        }
        this.createMoviesList(filmsDataChange(result.results), isNeedReset)
        }
    }

    async getMoviesUpcoming(pageNumber: number): Promise<void> {
        let isNeedReset = true;
        const result = await movieService.getMovieUpcoming(pageNumber);
        if (pageNumber > 1) {
            isNeedReset = false
        }
        this.createMoviesList(filmsDataChange(result.results), isNeedReset)
    }

    async getMoviesTopRated(pageNumber: number): Promise<void> {
        let isNeedReset = true;
        const result = await movieService.getMovieTopRated(pageNumber);
        if (pageNumber > 1) {
            isNeedReset = false
        }
        this.createMoviesList(filmsDataChange(result.results), isNeedReset)
    }

    createMoviesList(result: Array<movie>, isNeedReset: boolean) {
        const filmContainer: HTMLElement = document.getElementById('film-container') as HTMLElement;
        if (isNeedReset) {
            filmContainer.innerHTML = '';
        }
        result.forEach((movie: movie) => {
            const newEl = createMovieItem(movie.backdrop_path, movie.overview, movie.release_date, movie.id, 'main');
            filmContainer.append(newEl);
        });
        const isNeedRandomMovieChange: HTMLElement = document.getElementById('random-movie-name') as HTMLElement
        if (isNeedRandomMovieChange.innerText === '') {
            const randomFilm:movie = result[Math.floor(Math.random() * result.length)];
            this.createRandomFilm(randomFilm);
        }
    }

    createRandomFilm(film: movie) {
        const { title, overview, backdrop_path } = film
        const section: HTMLElement = document.getElementById('random-movie') as HTMLElement;
        section.setAttribute(
            "style", `background-image:url(https://image.tmdb.org/t/p/original/${backdrop_path});
                                        background-size: cover;
                                        background-position: center;`
        )
        const filmHeader: HTMLElement = document.getElementById('random-movie-name') as HTMLElement;
        const filmDescription: HTMLElement = document.getElementById('random-movie-description') as HTMLElement;
        filmHeader.innerText = title;
        filmDescription.innerText = overview;
    }

    async createFavoriteMoviesList(moviesId:Array<string>) {
        const favoriteMovies = [];

        for (let i = 0; i < moviesId.length; i++){
            const result = await movieService.getFavoriteMovie(moviesId[i]);
            favoriteMovies.push(result);
        }
        const favoriteFilmsContainer: HTMLElement = document.getElementById('favorite-movies') as HTMLElement;
        favoriteFilmsContainer.innerHTML = '';
        filmsDataChange(favoriteMovies).forEach((movie: movie) => {
            const newEl = createMovieItem(movie.backdrop_path, movie.overview, movie.release_date, movie.id, 'favorite');
            favoriteFilmsContainer.append(newEl);
        });

    }
}

export const moviesList = new MoviesList();