import { moviesList } from './scripts/components/MoviesList';
import { getAllLocalStorageItems, setLocalStorage } from './scripts/helpers/localStorageHelper';

export async function render(): Promise<void> {

    function makePageCounter() {
        let currentCount = 1;
        return {
            getNext: function() {
                return currentCount++;
            },

            getCurrent: function() {
                return currentCount;
            },

            reset: function() {
                currentCount = 1;
            }
        };
    }

    function activePageName() {
        let contentType = '';
        return {
            set: function(newContentType: string) {
                contentType = newContentType
            },

            get: function() {
                return contentType;
            },

            reset: function() {
                contentType = '';
            }
        };
    }

    const activePage = activePageName();
    const pageCounter = makePageCounter();

    const getContent = (contentType = 'popular') => {
        if (contentType === 'byKeywords'){
            const activePageNow = activePage.get();
            if (activePageNow !== 'byKeywords') {
                pageCounter.reset();
            }
            const currentPage = pageCounter.getCurrent();
            activePage.set('byKeywords');
            moviesList.getMoviesByKeywords(currentPage);
            pageCounter.getNext();
        }
        if (contentType === 'popular'){
            const activePageNow = activePage.get();
            if (activePageNow !== 'popular') {
                pageCounter.reset();
            }
            const currentPage = pageCounter.getCurrent();
            activePage.set('popular');
            moviesList.getMoviesPopular(currentPage);
            pageCounter.getNext();
        }
        if (contentType === 'upcoming'){
            const activePageNow = activePage.get();
            if (activePageNow !== 'upcoming') {
                pageCounter.reset();
            }
            const currentPage = pageCounter.getCurrent();
            activePage.set('upcoming');
            moviesList.getMoviesUpcoming(currentPage);
            pageCounter.getNext();
        }
        if (contentType === 'top'){
            const activePageNow = activePage.get();
            if (activePageNow !== 'top') {
                pageCounter.reset();
            }
            const currentPage = pageCounter.getCurrent();
            activePage.set('top');
            moviesList.getMoviesTopRated(currentPage);
            pageCounter.getNext();
        }
    };

    function nextPage() {
        const activePageNow = activePage.get();
        getContent(activePageNow)
    }

    const inputElement = document.getElementById('submit')
    if (inputElement) {
        inputElement.addEventListener('click', () => getContent('byKeywords'), false);
    }

    const popularElement = document.getElementById('popular');
    if (popularElement) {
        popularElement.addEventListener('click', () => getContent('popular'), false);
    }

    const upcomingElement = document.getElementById('upcoming');
    if (upcomingElement) {
        upcomingElement.addEventListener('click', () => getContent('upcoming'), false);
    }

    const topRatedElement = document.getElementById('top_rated');
    if (topRatedElement) {
        topRatedElement.addEventListener('click', () => getContent('top'), false);
    }

    const nextPageElement = document.getElementById('load-more');
    if (nextPageElement) {
        nextPageElement.addEventListener('click', nextPage, false);
    }


    const mainFilmContainerElement = document.querySelector('#film-container');
    if (mainFilmContainerElement) {
        mainFilmContainerElement.addEventListener('click', (e:Event) =>
            setLocalStorage(e, 'main')
        );
    }

    const favoriteBlockContainerElement = document.querySelector('#favorite-movies');
    if (favoriteBlockContainerElement) {
        favoriteBlockContainerElement.addEventListener('click', (e:Event) =>
            setLocalStorage(e, 'favorite')
        );
    }

    const showFavoriteMoviesElement = document.querySelector('.navbar-toggler')
    if(showFavoriteMoviesElement) {
        showFavoriteMoviesElement.addEventListener('click', getAllLocalStorageItems);
    }

    const closeFavoriteMoviesElement = document.querySelector('.btn-close')
    if(closeFavoriteMoviesElement) {
        closeFavoriteMoviesElement.addEventListener('click', getAllLocalStorageItems);
    }

    getContent();
}
